Currently, you will need Unity v.2019.1.5f1 installed for the game to run and work. 


The code is located in Assets/Scripts, where all the code required to run the game
is stored.
The Notebook is stored in the Evidence folder, along with some videos and screenshots 
of the progress of this project.

The Project is split into parts to save space on upload. 
Download the Volumes 1-4 and extract the contents into a folder.

UPDATE 1.1: Spam bugs fixed, X Co-ordinate bugs fixed, rotations work when the player is behind the AI and vice versa.
Build still bugged, will demonstrate in Unity.

UPDATE 1.1.1: New Sounds for hitting and falling over.

To play the full game: Start in the MainMenu Scene in Unity. There might be an issue in the file names, as some 
may appear in lower case, to fix this, rename the files beginning with a lower case letter with it's capital case
alternative.

Controls are: 
Move right: right arrow
Move left: left arrow
Move towards camera: up arrow
Move away camera: down arrow
Punch: Z
Kick: X
Block: C
Taunt: V/B

ACKNOWLEDGEMENTS:

Models and different motion data not captured via Motion Capture were found on the Unity Asset Store.