﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //public variables
    public float speed = 5.0f;
    public float groundDist = 0.2f;
    public float jump = 6.0f;
    public LayerMask grnd;


    //private variables
    private Rigidbody rBody;
    private Vector3 input = Vector3.zero;
    private bool isGrounded = true;
    private CharacterController pControl;
    private Transform groundCheck;
    Vector3 origin;

    void Start()
    {
        //pControl = GetComponent<CharacterController>();
        rBody = GetComponent<Rigidbody>(); //get the rigid body component
        origin = new Vector3(rBody.transform.position.x, rBody.transform.position.y, rBody.transform.position.z);
    }

    //private void Update()
    //{
    //    isGrounded = Physics.CheckCapsule(groundCheck.position, groundDist, grnd, QueryTriggerInteraction.Ignore);
    //}

    private void FixedUpdate()
    {



        //floats for movement
        float moveHor = Input.GetAxis("Horizontal"); //horizontal movement, rotating left and right
        float moveVert = Input.GetAxis("Vertical"); //vertical movement, moving forwards and backwards
        float lookHor = Input.GetAxis("Mouse Y"); //horizontal mouse movement, look left and right
        float lookVert = Input.GetAxis("Mouse X");//vertical mouse movement, look up and down



        //transform code for actual movement

        //transform.Translate(moveHor, 0, moveVert);
        //transform.Rotate(lookHor, lookVert, 0);

        if(Input.GetKey(KeyCode.LeftShift))
        {
            speed = 6.0f;
        }
        else
        {
            speed = 5.0f;
        }

        Vector3 movement = new Vector3(moveHor, 0, moveVert).normalized;
        
        rBody.AddForce((movement * moveHor) + (movement*moveVert) * speed/Time.deltaTime);

        //jump code
        if (Input.GetKeyDown(KeyCode.Space)) //if space is pressed
        {
            rBody.AddForce(Vector3.up * jump, ForceMode.Impulse);
        }

        //reset position
        if (Input.GetKeyDown(KeyCode.R)) //if r is pressed
        {
            rBody.transform.position = origin;
        }
    }

    private bool onFloor()
    {
        return false;   
    }

}
