﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    //public variables
    public float speed = 5.0f;
    public float turnSpeed = 200.0f;
    public float stopDistance = 1.0f;
    

    ////hidden player attributes
    //[HideInInspector] public int playerNum; //the player's number for the manager
    //[HideInInspector] public int numOfWins; //amount of round wins the player has
    //[HideInInspector] public string playerText; //gets the player text.

    //private variables
    private Rigidbody controller;
    private Transform player;
    private CharAnim enemyAnim;
    private float attackDistance = 1.0f;
    private float chaseAfterAttack = 1.0f;
    private float currentAttackTime;
    private float defaultAttackTime = 2.0f;
    private float damping = 1.0f;
    private float yRotation = -180.0f;
    private bool followPlayer, attackPlayer;
    private PlayerAttack playerAttack;

    
    

    // Use this for initialization
    void Awake()
    {
        enemyAnim =GetComponentInChildren<CharAnim>();
        controller = GetComponent<Rigidbody>();
        playerAttack = GameObject.FindWithTag(Tags.PLAYER_TAG).GetComponent<PlayerAttack>();
        player = GameObject.FindWithTag(Tags.PLAYER_TAG).transform; //find the player
    }

    void Start () {
        followPlayer = true;
        currentAttackTime = defaultAttackTime;
	}

   
    // Update is called once per frame
    void Update () {
        EnemyAttack();
        EnemyDefend();
    }

    void FixedUpdate()
    {
        FollowTarget();
    }

    void FollowTarget()
    {
        if(!followPlayer)
        {
            return; //exit from function if the AI is no longer following the player
        }
        //Debug.Log(Vector3.Distance(transform.position, player.position)); //debug for printing distance
        if(Vector3.Distance(transform.position, player.position)  > attackDistance)
        {
            var lookPosition = player.position - transform.position;
            lookPosition.y = 0;

            if (transform.position.z < player.position.z)
            {

                transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                //var lookRotation = Quaternion.LookRotation(lookPosition);
                //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * damping);

            }
            //look at function to fix y position
            else
            {
                transform.rotation = Quaternion.Euler(0.0f,yRotation, 0.0f);
                //var lookRotation = Quaternion.LookRotation(lookPosition);
                //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * damping);

            }

            //move forward
            controller.velocity = transform.forward * speed;
           
            if(controller.velocity.sqrMagnitude != 0)
            {
                enemyAnim.WalkCheck(true);
            }
            //print("Following Player");
        }
        else if (Vector3.Distance(transform.position, player.position) < attackDistance)
        {
            controller.velocity = Vector3.zero;
            enemyAnim.WalkCheck(false);
            //print("Stop following Player");

            followPlayer = false;
            attackPlayer = true;
        }
    }

    void EnemyAttack()
    {
        if(!attackPlayer)
        {
            return;
        }

        currentAttackTime += Time.deltaTime;
        if(currentAttackTime > defaultAttackTime)
        {
            enemyAnim.EnemyAttack(Random.Range(0, 3));
            currentAttackTime = 0f;
            //print("Attacking Player");
        }


        if(Vector3.Distance(transform.position, player.position) > attackDistance + chaseAfterAttack)
        {
            attackPlayer = false;
            followPlayer = true;
            //print("Re-following Player");
        }

    }

    //public void Reset()
    //{
    //    transform.position = spawnPoint.position;
    //    transform.rotation = spawnPoint.rotation;

    //}

    public bool EnemyDefend()
    {
        int blockCondition = Random.Range(0, 10);
        if (playerAttack.PlayerIsAttacking() && blockCondition >= 5)
        {
            enemyAnim.BlockCheck(true);
            print("Blocking");
            return true;
        }
        else
        {
            enemyAnim.BlockCheck(false);
            return false;
        }

    }
}
    