﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public float baseHealth = 10.0f;
    private float currentHealth;


	// Use this for initialization
	void Start () {
        currentHealth = baseHealth;
		
	}
	
    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        if (currentHealth < 0)
        {
            Kill();
        }


    }

	// Update is called once per frame
	void Kill() {
        Destroy(gameObject);
    }
}
