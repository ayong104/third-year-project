﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{

    //private
    private GameObject[] charList;
    private int index;


    // Start is called before the first frame update
    void Start()
    {
        index = PlayerPrefs.GetInt("CharacterSelected");
        charList = new GameObject[transform.childCount];

        //putting each character model in the character list array
        for(int i = 0; i < transform.childCount; i++)
        {
            charList[i] = transform.GetChild(i).gameObject;
        }

        //Toggle render
        foreach(GameObject characterObject in charList)
        {
            characterObject.SetActive(false);
        }

        //Enabling first char in the list
        if(charList[index])
        {
            charList[index].SetActive(true);
        }

    }


    public void ToggleLeftSelection()
    {
        //toggle character model
        charList[index].SetActive(false);

        index--;
        if(index < 0)
        {
            index = charList.Length - 1;
        }
        charList[index].SetActive(true);

    }

    public void ToggleRightSelection()
    {
        //toggle character model
        charList[index].SetActive(false);

        index++;
        if (index == charList.Length)
        {
            index = 0;
        }
        charList[index].SetActive(true);

    }
   
    public void ConfirmChar()
    {
        PlayerPrefs.SetInt("CharacterSelected", index);
        SceneManager.LoadScene("Level1");
    }
}
