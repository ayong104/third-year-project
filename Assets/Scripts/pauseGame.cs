﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseGame : MonoBehaviour {

    // Use this for initialization
    public Transform pauseCanvas;
    public Transform player;
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

	}

    public void Pause()
    {
        if (pauseCanvas.gameObject.activeInHierarchy == false)
        {
            pauseCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            player.GetComponent<CharacterController>().enabled = false;
        }
        else
        {
            pauseCanvas.gameObject.SetActive(false);
            Time.timeScale = 1;
            player.GetComponent<CharacterController>().enabled = true;
        }
    }
}
