﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackChecks : MonoBehaviour
{

    //public variables
    public LayerMask collisionLayer; //layer where the collisions will be categorised in
    public float attackRadius = 1.0f;
    public float attackDamage = 2.0f;

    public bool isPlayer, isEnemy;
    public GameObject hitFX;

    private PlayerAttack playerAttack;
    private EnemyController enemyAttack;

    void Awake()
    {
        playerAttack = GameObject.FindWithTag(Tags.PLAYER_TAG).GetComponent<PlayerAttack>();
        enemyAttack = GameObject.FindWithTag(Tags.ENEMY_TAG).GetComponent<EnemyController>();
        Debug.Log("Calling controllers");
    }

    // Update is called once per frame
    void Update()
    {
        CollisionDetection();
        
    }

    void CollisionDetection()
    {
        Collider[] hit = Physics.OverlapSphere(transform.position, attackRadius, collisionLayer);
        if (hit.Length > 0)
        {
            if (isPlayer)
            {
                //&& (playerAttack.Block() || enemyAttack.EnemyDefend())
                //Vector3 hitFXPos = hit[0].transform.position;
                // hitFXPos.y += 1.3f;

                // if(hit[0].transform.forward.x > 0)
                // {
                //      hitFXPos.x += 0.3f;
                // }
                // else if(hit[0].transform.forward.x < 0)
                // {
                //    hitFXPos.x -= 0.3f; 
                // }


                //print("Current Hit: " + hit[0].gameObject.name);
                if (!enemyAttack.EnemyDefend())
                {
                    if ((gameObject.CompareTag(Tags.LEFT_ARM_TAG) || gameObject.CompareTag(Tags.LEFT_LEG_TAG)))
                    {
                        hit[0].GetComponent<Health>().ApplyDamage(attackDamage, true);
                    }
                    else
                    {
                        hit[0].GetComponent<Health>().ApplyDamage(attackDamage, false);
                    }
                }
                else
                {
                    hit[0].GetComponent<Health>().ApplyDamage(0, false);
                }


            }

            if (isEnemy)
            {
                //print("Enemy has Hit: " + hit[0].gameObject.name);
                if (!playerAttack.Block())
                {
                    hit[0].GetComponent<Health>().ApplyDamage(attackDamage, false);
                }
                else
                {
                    hit[0].GetComponent<Health>().ApplyDamage(0, false);
                }

            }

        }
        gameObject.SetActive(false);

    }
}
