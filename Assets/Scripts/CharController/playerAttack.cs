﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ComboState
{
    NONE,
    PUNCH1,
    PUNCH2,
    PUNCH3,
    KICK1,
    KICK2
}


public class PlayerAttack : MonoBehaviour
{
    /** public variables **/
    public bool isBlock;
    //public int playerNumber = 1; //to be set by the fighter manager


    /** private variables **/
    private CharAnim playerAnim;
    private bool activateTimerToReset;
    private bool cooldown;
    private float currentComboTimer;
    private ComboState currentComboState;
    private bool isAttacking;
    private Player player;
    private float attackTimer = 0; //cooldown timer
    private bool attackTimerReached = false;

    [SerializeField]
    private float defaultComboTimer = 3.0f;
    private float defaultCooldownTimer = 2.0f;
    //[SerializeField]
    //private float attackDelay = 1.0f;

    // Start is called before the first frame update
    void Awake()
    {
        player = GetComponent<Player>();
        playerAnim = GetComponentInChildren<CharAnim>();
    }


    void Start()
    {
        isAttacking = false;
        currentComboTimer = defaultComboTimer;
        currentComboState = ComboState.NONE;
    }

    // Update is called once per frame
    void Update()
    {
        //ResetDelay();
        ComboPunch();
        ComboKick();
        Block();
        PlayerIsAttacking();
        ResetComboState();
        Taunt();
    }

    void Taunt()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            playerAnim.TauntCheck();
        }
    }

    void ComboPunch()
    {
        if (Input.GetKeyDown(KeyCode.Z)) //if punch key is pressed
        {
            if (cooldown == false)
            {
                isAttacking = true;

                if (currentComboState == ComboState.PUNCH3 ||
                      currentComboState == ComboState.KICK1 ||
                      currentComboState == ComboState.KICK2)
                { return; }


                currentComboState++;
                activateTimerToReset = true;
                currentComboTimer = defaultComboTimer;


                if (currentComboState == ComboState.PUNCH1)
                {
                    playerAnim.Punch1Check();
                }
                if (currentComboState == ComboState.PUNCH2)
                {
                    playerAnim.Punch2Check();
                }
                if (currentComboState == ComboState.PUNCH3)
                {
                    playerAnim.Punch3Check();
                }

                if (!player.IsOnGround())
                {
                    playerAnim.AirPunchCheck();
                }
                Invoke("ResetCooldown", 0.4f);
                cooldown = true;
            }
        }
        else
        {
            isAttacking = false;
        } 
    }

    void ComboKick()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (cooldown == false)
            {
                isAttacking = true;

                if (currentComboState == ComboState.KICK2 ||
                    currentComboState == ComboState.PUNCH3)
                { return; }

                if (currentComboState == ComboState.NONE ||
                    currentComboState == ComboState.PUNCH1 ||
                    currentComboState == ComboState.PUNCH2)
                {
                    currentComboState = ComboState.KICK1;
                }
                else if (currentComboState == ComboState.KICK1)
                {
                    currentComboState++;
                }

                activateTimerToReset = true;
                currentComboTimer = defaultComboTimer;

                if (currentComboState == ComboState.KICK1)
                {
                    playerAnim.Kick1Check();
                }
                if (currentComboState == ComboState.KICK2)
                {
                    playerAnim.Kick2Check();
                }

                if (!player.IsOnGround())
                {
                    playerAnim.AirKickCheck();
                }
                Invoke("ResetCooldown", 0.5f);
                cooldown = true;
            }
        }
        else
        {
            isAttacking = false;
        }
        
    }

    //void ResetDelay()
    //{
    //    if((isAttacking || isBlock) && attackDelay > 0)
    //    {
    //        attackDelay -= Time.deltaTime;
    //    }
    //    if(attackDelay < 0)
    //    {
    //        attackDelay = 0;
    //        isAttacking = false;
    //        isBlock = false;
    //    }



    //}

   public bool PlayerIsAttacking()
   {
        if(isAttacking)
        {
            //print("Is attacking");
            return true;
        }
        else
        {
            return false;
        }
   }
    void ResetComboState()
    {
        if(activateTimerToReset)
        {
            currentComboTimer -= Time.deltaTime;
            if (currentComboTimer <= 0f)
            {
                currentComboState = ComboState.NONE;
                activateTimerToReset = false;
                currentComboTimer = defaultComboTimer;
            }
        }
    }

    void ResetCooldown()
    {
        cooldown = false; 
    }

    public bool Block()
    {
        if (Input.GetKey(KeyCode.C))
        {
            if(cooldown == false)
            {
                Invoke("ResetCooldown", 1.5f);
                cooldown = true;
                playerAnim.BlockCheck(true);
                
            }
            return true;
        }
        else
        {
            playerAnim.BlockCheck(false);
            return false;
        }

    }

  

}
