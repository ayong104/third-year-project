﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnim : MonoBehaviour
{

    /***PRIVATE VARIABLES***/
    private Animator animControl;
   


    // Use this for initialization
    void Awake()
    {
        animControl = GetComponent<Animator>();
       
        //isAttack = localAttack;

    }

    
    public void WalkCheck(bool move)
    {
        animControl.SetBool(AnimationTags.MOVEMENT, move);
    }

    //punch checks
    public void Punch1Check()
    {
        animControl.SetTrigger(AnimationTags.PUNCH_1_TRIGGER);
    }
    public void Punch2Check()
    {
        animControl.SetTrigger(AnimationTags.PUNCH_2_TRIGGER);
    }
    public void Punch3Check()
    {
        animControl.SetTrigger(AnimationTags.PUNCH_3_TRIGGER);
    }
    public void AirPunchCheck()
    {
        animControl.SetTrigger(AnimationTags.AIR_PUNCH_TRIGGER);
    }
    
    //kick checks
    public void Kick1Check()
    {
        animControl.SetTrigger(AnimationTags.KICK_1_TRIGGER);
    }
    public void Kick2Check()
    {
        animControl.SetTrigger(AnimationTags.KICK_2_TRIGGER);
    }
    public void AirKickCheck()
    {
        animControl.SetTrigger(AnimationTags.AIR_KICK_TRIGGER);
    }

    //block check
    public void BlockCheck(bool block)
    {
        animControl.SetBool(AnimationTags.BLOCK, block);
    }

    //jump check
    public void JumpCheck()
    {
        animControl.SetTrigger(AnimationTags.JUMP_TRIGGER);
    }

    //taunt check
    public void TauntCheck()
    {
        animControl.SetTrigger(AnimationTags.TAUNT_TRIGGER);
    }
    /** Enemy Animations **/

    public void EnemyAttack(int attack)
    {
        if (attack == 0)
        {
            animControl.SetTrigger(AnimationTags.ATTACK_1_TRIGGER);
        }
        if (attack == 1)
        {
            animControl.SetTrigger(AnimationTags.ATTACK_2_TRIGGER);
        }
        if (attack == 2)
        {
            animControl.SetTrigger(AnimationTags.ATTACK_3_TRIGGER);
        }
    }

    

    public void PlayIdleAnim()
    {
        animControl.Play(AnimationTags.IDLE_ANIMATION);
    }

    public void KnockDown()
    {
        animControl.SetTrigger(AnimationTags.KNOCK_DOWN_TRIGGER);
    }
    public void StandUp()
    {
        animControl.SetTrigger(AnimationTags.STAND_UP_TRIGGER);
    }
    public void Hit()
    {
        animControl.SetTrigger(AnimationTags.HIT_TRIGGER);
    }
    public void Death()
    {
        animControl.SetTrigger(AnimationTags.DEATH_TRIGGER);
    }

    
}



