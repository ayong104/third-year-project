﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    //public
    public Text timeText;
    public float maxTime;
    public float timeLeft;
    




    // Use this for initialization
    void Start () {
        timeLeft = maxTime;
	}
	
	// Update is called once per frame
	void Update () {
       

        timeLeft = timeLeft - Time.deltaTime;
        timeText.text = ("" + Mathf.Round(timeLeft));
        if(timeLeft < 0)
        {
            timeLeft = 0;
            print("Time's Up!");
        }

	}
}
