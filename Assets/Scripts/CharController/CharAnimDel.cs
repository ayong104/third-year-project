﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimDel : MonoBehaviour
{

    //public fields
    public GameObject leftArmAttackPoint, rightArmAttackPoint, leftLegAttackPoint, rightLegAttackPoint;
    public float standUpTimer = 2.0f;

    //private fields
    private CharAnim animScript;
    private AudioSource audioSource;
    private EnemyController enemyMovement;
    private PlayerAttack playerAttack;

    [SerializeField]
    private AudioClip whooshClip, fallingClip, groundHitClip, deathClip;

    void Awake()
    {
        animScript = GetComponent<CharAnim>();
        audioSource = GetComponent<AudioSource>();
        if(gameObject.CompareTag(Tags.ENEMY_TAG))
        {
            enemyMovement = GetComponentInParent<EnemyController>();
            Debug.Log("Assigned Enemy Controller");
        }
        else
        {
            playerAttack = GetComponentInParent<PlayerAttack>();
            Debug.Log("Assigned Player Controller");
        }
    }

    //enemy movements
    void MovementDisabled()
    {
        enemyMovement.enabled = false;

        //setting enemy parent to default to prevent overkill attacks
        transform.parent.gameObject.layer = 0;
    }

    void MovementEnabled()
    {
        enemyMovement.enabled = true;

        //re-enabling layer for player to attack enemy
        transform.parent.gameObject.layer = 10;
    }

    void AttackDisabled()
    {
        playerAttack.enabled = false;
    }

    void AttackEnabled()
    {
        playerAttack.enabled = true;
    }

    //left arm
    void LeftArmAttackEnabled()
    {
        leftArmAttackPoint.SetActive(true);
    }

    void LeftArmAttackDisabled()
    {
        if (leftArmAttackPoint.activeInHierarchy)
        {
            leftArmAttackPoint.SetActive(false);
        }

    }

    //right arm
    void RightArmAttackEnabled()
    {
        rightArmAttackPoint.SetActive(true);
    }

    void RightArmAttackDisabled()
    {
        if (rightArmAttackPoint.activeInHierarchy)
        {
            rightArmAttackPoint.SetActive(false);
        }

    }

    //left leg
    void LeftLegAttackEnabled()
    {
        leftLegAttackPoint.SetActive(true);
    }

    void LeftLegAttackDisabled()
    {
        if (leftLegAttackPoint.activeInHierarchy)
        {
            leftLegAttackPoint.SetActive(false);
        }

    }

    //right leg
    void RightLegAttackEnabled()
    {
        rightLegAttackPoint.SetActive(true);
    }

    void RightLegAttackDisabled()
    {
        if (rightLegAttackPoint.activeInHierarchy)
        {
            rightLegAttackPoint.SetActive(false);
        }

    }

    //left tags
    void TagLeftArm()
    {
        leftArmAttackPoint.tag = Tags.LEFT_ARM_TAG;
    }


    void UntagLeftArm()
    {
        leftArmAttackPoint.tag = Tags.UNTAGGED_TAG;

    }

    void TagLeftLeg()
    {
        leftLegAttackPoint.tag = Tags.LEFT_LEG_TAG;
    }


    void UntagLeftLeg()
    {
        leftLegAttackPoint.tag = Tags.UNTAGGED_TAG;

    }

    void EnemyStandUp()
    {
        StartCoroutine(StandUpAfterTime());
    }

    IEnumerator StandUpAfterTime()
    {
        yield return new WaitForSeconds(standUpTimer);
        animScript.StandUp();
    }

    void AttackFXSound()
    {
        audioSource.volume = 0.2f;
        audioSource.clip = whooshClip;
        audioSource.Play();

    }

     void CharDeadSound()
     {
        audioSource.volume = 1.0f;
        audioSource.clip = deathClip;
        audioSource.Play();
     }

    void EnemyKnockDownSound()
    {
        audioSource.clip = fallingClip;
        audioSource.Play();
    }

    void EnemyHitGroundSound()
    {
        audioSource.clip = groundHitClip;
        audioSource.Play();

    }

    void CharacterDead()
    {
        Invoke("DeactivateGameObject", 2.0f);
    }

    void DeactivateGameObject()
    {
        EnemySpawner.instance.SpawnEnemy();
        gameObject.SetActive(false);

    }
}
