﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    private Image healthImageUI;
    private bool uiReset;
    public bool isPlayerUI;
    public bool isEnemUI;
    void Awake()
    {
        if(isPlayerUI)
        {
            healthImageUI = GameObject.FindWithTag(Tags.HEALTH_UI_PLAYER).GetComponent<Image>();
        }
        else if(isEnemUI)
        {
            healthImageUI = GameObject.FindWithTag(Tags.HEALTH_UI_ENEMY).GetComponent<Image>();
        }
       
    }


   public void dispHealth(float healthVal)
    {
        healthVal /= 100.0f;
        if(healthVal < 0.0f)
        {
            healthVal = 0.0f;
            uiReset = true;
        }
        if(uiReset)
        {
            healthVal /= 100.0f;
        }
        
        healthImageUI.fillAmount = healthVal;
    }
}
