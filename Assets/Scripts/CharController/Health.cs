﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    //public variables
    public float health = 100.0f;
    public bool isPlayer;

    //private variables
    private CharAnim animScript;
    private EnemyController enemyMovement;
    private bool charDead;
    private HealthUI healthUIPlayer, healthUIEnem;

    //[SerializeField]
    //private int player1Round, player2round;
    void Awake()
    {
        animScript = GetComponentInChildren<CharAnim>();
        healthUIEnem = GetComponent<HealthUI>();
        if(isPlayer)
        {
            healthUIPlayer = GetComponent<HealthUI>();
        }
    }


   public void ApplyDamage(float damage, bool knockDown)
    {
        if (charDead)
            return;

        health -= damage;


        //UI section
        


        if(health <= 0.0f)
        {
            animScript.Death();
            charDead = true;

            if(isPlayer)
            {
                GameObject.FindWithTag(Tags.ENEMY_TAG).GetComponent<EnemyController>().enabled = false;
                GameObject.FindWithTag(Tags.PLAYER_TAG).GetComponent<Player>().enabled = false;

            }
            else if(!isPlayer)
            {
                //player1Round++;

            }
            return;

        }

        if (isPlayer)
        {
            healthUIPlayer.dispHealth(health);
            if (Random.Range(0, 3) > 1)
            {
                animScript.Hit();

            }
        }

        else if (!isPlayer)
        {
            healthUIEnem.dispHealth(health);
            if (knockDown)
            {
                if(Random.Range(0,2) > 0)
                {
                    animScript.KnockDown();
                }
            }
            else
            {
                if (Random.Range(0, 3) > 1)
                {
                    animScript.Hit();

                }  

            }

        }

    }




}
