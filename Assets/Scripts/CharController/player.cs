﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {



    //public variables
    
    public float speed = 5.0f;
    public float zSpeed = 2.5f;
    public float rotateSpeed = 5.0f;
    public float groundDist = 0.2f;
    public float jump = 3.0f;
    public float gravity = 10.0f;
    //public float health = 100.0f;
    //public Transform head;
  

    ////hidden player attributes
    //[HideInInspector] public int playerNum; //the player's number for the manager
    //[HideInInspector] public int numOfWins; //amount of round wins the player has
    //[HideInInspector] public string playerText; //gets the player text.


    //[Range(0, 1)]
    //public float airControlPercent;
    

    //private variables
    private Vector3 inputDir = Vector3.zero;
    private bool isGrounded = true;
    private Rigidbody pControl;
    private CharAnim playerAnim;
    private Transform enemy;
    private float yRotation = -180f;
    private float animationSpeedPercent;

    // Use this for initialization
    private void Start()
    {
        enemy = GameObject.FindWithTag(Tags.ENEMY_TAG).transform;
    }

    void Awake () {
        pControl = GetComponent<Rigidbody>();
        playerAnim = GetComponentInChildren<CharAnim>();
        
    }

    void Update()
    {
        QuitCheck();
    }

    void FixedUpdate() {
        IsOnGround();
        MovementCheck();
        AnimatePlayerWalk();
        PlayerRotation();
    }

    void QuitCheck()
    {
        //quit the game on escape
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    
    public bool IsOnGround()
    {
        return Physics.Raycast(transform.position, -Vector3.up, groundDist + 0.1f);
    }

    // Update is called once per frame
    void MovementCheck() {
        float moveHor = Input.GetAxisRaw("Horizontal"); //horizontal movement, rotating left and right
        float moveVert = 0;// Input.GetAxisRaw("Vertical"); //vertical movement, moving forwards and backwards

        pControl.velocity = new Vector3(moveVert* (-speed), pControl.velocity.y, moveHor * (-zSpeed));

        //old movement
      //  inputDir = new Vector3(moveVert, 0, -moveHor);
       // inputDir = transform.TransformDirection(inputDir);
        //inputDir *= speed;
       // pControl.velocity = (inputDir * Time.deltaTime);


        if (IsOnGround())
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                pControl.AddForce(new Vector3(0, jump, 0), ForceMode.Impulse);
                playerAnim.JumpCheck();
            }
             //pControl.velocity = new Vector3(moveVert* (-speed), pControl.velocity.y, moveHor * (-zSpeed));
        }
        //else
        //{
        //    inputDir = new Vector3(moveVert, inputDir.y, -moveHor);
        //    inputDir = transform.TransformDirection(inputDir);
        //    inputDir.x *= speed;
        //    inputDir.z *= speed;
        //}

        //inputDir.y -= gravity * Time.deltaTime;
        
    }

    void PlayerRotation()
    {
        float moveHor = Input.GetAxisRaw("Horizontal");
        if (enemy.position.z < pControl.position.z) //if the enemy is "behind" of the player, rotate the player to face the enemy
        {
            transform.rotation = Quaternion.Euler(0.0f, yRotation, 0.0f);
            //print("Going forwards");
        }
        else
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            //print("Going backwards");
        }

    }

    //public void Reset() //used for resetting the player, doesn't work
    //{
    //    transform.position = spawnPoint.position;
    //    transform.rotation = spawnPoint.rotation;
    //}
    void AnimatePlayerWalk()
    {
        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {

            playerAnim.WalkCheck(true);
        }
        else
        {
            playerAnim.WalkCheck(false);
        }
    }
   
}
